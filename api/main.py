from flask import Flask
from flask import request, jsonify
from flask_expects_json import expects_json
from init import Whatsapp
import os, requests

app = Flask(__name__, static_url_path='/static')
wa = Whatsapp()
token = "eyJkZXZlbG9wZXIiOiJzaW1pcHJhbWJvcyJ9"
schema = {
    "type":"object",
    "properties":{
        "id":{"type":"string"},
        "msg":{"type":"string"},
        "token":{"type":"string"},
    },
    "required":["id","msg","token"]
}
config_schema = {
    "type":"object",
    "properties":{
        "token":{"type":"string"},
        "endpoint_url":{"type":"string"}
    },
    "required":["token","endpoint_url"]
}

@app.route('/config', methods=['POST'])
@expects_json(config_schema)
def set_config():
    params = request.json
    if params['token'] == token:
        try:
            f = open('endpoint.conf', 'w+')
            w = f.write(params['endpoint_url'])
            f.close()
            return jsonify({"success":"endpoint_url was changed, please restart the server!"}), 201
        except:
            return jsonify({"error":"something when wrong, please try again!"}), 400
    else:
        return jsonify({"error":"Unauthorized"}), 401

@app.route('/scan')
def start_server():
    img = ''
    for root, dirs, files in os.walk("static"):  
        for filename in files:
            img = filename
    html = '<img src=/static/%s>' %img
    print(html)
    return html

@app.route('/send', methods=['POST'])
@expects_json(schema)
def send():
    params = request.json
    if params['token'] == token:
        try:
            wa.send_message(params['id'], params['msg'])
        except:
            return jsonify(success=False), 400
        return jsonify(success=True), 201
    else:
        return jsonify({"error":"Unauthorized"}), 401

@app.route('/')
def index():
    return 'welcome to whatsapp api'

if __name__=='__main__':
    app.run(host="0.0.0.0",port=5000, debug=True)
