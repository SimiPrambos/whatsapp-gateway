import urllib.request
import json

class Sessions(object):
    grid_url = "http://127.0.0.1:4444/wd/hub"

    def get_session(self):
        id = None
        capname = None

        sessions_req = urllib.request.urlopen(self.grid_url + "/sessions")
        sessions_data = sessions_req.read()
        sessions_encoding = sessions_req.info().get_content_charset('utf-8')

        sessions = json.loads(sessions_data.decode(sessions_encoding))

        for session in sessions["value"]:
            id = session["id"]
            capname = session["capabilities"]["browserName"]
        return [id, capname]

    def create_session(self, session_id, executor_url):
        from selenium.webdriver.remote.webdriver import WebDriver as RemoteWebDriver

        # Save the original function, so we can revert our patch
        org_command_execute = RemoteWebDriver.execute

        def new_command_execute(self, command, params=None):
            if command == "newSession":
                # Mock the response
                return {'success': 0, 'value': None, 'sessionId': session_id}
            else:
                return org_command_execute(self, command, params)

        # Patch the function before creating the driver object
        RemoteWebDriver.execute = new_command_execute

        new_driver = RemoteWebDriver.Remote(command_executor=executor_url, desired_capabilities={})
        new_driver.session_id = session_id

        # Replace the patched function with original function
        RemoteWebDriver.execute = org_command_execute

        return new_driver
            