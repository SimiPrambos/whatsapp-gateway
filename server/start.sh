echo "starting .."
Xvfb :10 -ac &
export DISPLAY=:10
export PATH=$PATH:/home/dev/app/server/geckodriver
java -jar server.jar
killall Xvfb
killall java
killall geckodriver
echo "goodbye..."
